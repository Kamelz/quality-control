#Leads 
- id
- partner_name
- created_at 
- f-name 
- l-name 
- email 
- country 
- phone with code 
- age 
- no_valid
- quality_score 
- feedback default "No feedback yet"

#Users

- id
- name
- email
- funds
- created_at
- daily_leads
- total_leads
- action_price
- countries_target
- quality_score_target

#Countries
- id
- country

#country_partner
- id
- country_id
- partner_id


#messages
- id
- message
- partner_id


on production import sql/country.mysql.sql