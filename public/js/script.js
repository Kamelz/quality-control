	$(document).ready(function(){
		$("#wake-up").hide();
	});

	function markUserMessagesAsRead(){
	    $.ajax({
            url: '/mark-as-read',
            type: 'POST',
            data: {"_token": $('meta[name="csrf-token"]').attr('content')}
        });
	}

	function animateToTop(elementID){
		$('html, body').animate({
		    scrollTop: $("#"+elementID).offset().top
		}, 1000);
	}

