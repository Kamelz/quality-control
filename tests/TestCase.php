<?php

namespace Tests;
use DB;
use Artisan;
use App\Country;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
	* Setup the test environment.
	*
	* @return void
	*/
    protected function setUp(){
		
		parent::setUp();

		$this->countrySeeder();
		$this->withoutExceptionHandling();	
    }

    public function countrySeeder(){
    	$countriesNotEmpty = Country::count();
		if($countriesNotEmpty === 0){
			$path = 'sql/countries.sql';
			// dd("../../".__file__."/sql/countries.sql");
			DB::unprepared(file_get_contents(base_path($path)));
		}
    }
}
