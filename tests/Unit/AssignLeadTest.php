<?php

namespace Tests\Unit;

use App\Lead;
use App\User;
use Tests\TestCase;
use App\AssignQueue;
use App\Service\AssignLead;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AssignLeadTest extends TestCase
{
	use RefreshDatabase;
	
	protected $lead;
	protected $assign;
	
	/**
	* Setup the test environment.
	*
	* @return void
	*/
    protected function setUp(){
		parent::setUp();
		$this->lead = factory(Lead::class)->create(['user_id'=>null,'quality_score' => 4]);
        
		$this->assign = new AssignLead($this->lead);
    }

 /**
     *
     * @test
     */
    public function it_checks_if_assign_queue_table_is_not_empty(){
    	$user = factory(User::class)->create(); 
    
    	
    	AssignQueue::create([
    		'user_id' => $user,
    		'quality_score' =>4
    	]);  

		$this->assertTrue($this->assign->isAssignQueueNotEmpty());
    } 

 /**
     *
     * @test
     */
    public function it_can_add_matched_users_to_queue_table(){

    	$userWillBeAddedToQueue1 = factory(User::class)->create([
      		'quality_score_target_from' => 1,
            'quality_score_target_to' => 4,
    	]); 
    	
    	$userWillBeAddedToQueue2 = factory(User::class)->create([
      		'quality_score_target_from' => 4,
            'quality_score_target_to' => 5,
    	]); 

    	$userWillNotBeAddedToQueue = factory(User::class)->create([
    		'quality_score_target_from' => 5,
    		'quality_score_target_to' => 10,
    	]);
   

		$this->assign->addMatchedUsersToQueue();

		$this->assertEquals(2,AssignQueue::count());
	
    	$this->assertDatabaseHas('assign_queues',[
    		'user_id' => $userWillBeAddedToQueue1->id,
    		'quality_score' => 4
    	]);
    	
    	$this->assertDatabaseHas('assign_queues',[
    		'user_id' => $userWillBeAddedToQueue2->id,
    		'quality_score' => 4
    	]);

    } 
    
    /**
     *
     * @test
     */
    public function it_can_assign_lead(){

    	$userWillBePoped = factory(User::class)->create([
    		'total_leads' => 0,
    		'funds' => 10,
    	]);

    	$userWillNotBePoped = factory(User::class)->create([
    		'total_leads' => 0,
    		'funds' => 10,
    	]); 
    
    	$userWithDiffrentTarget = factory(User::class)->create([
    		'total_leads' => 0,
    		'funds' => 10,
    	]); 
    	
    	AssignQueue::create([
    		'user_id' => $userWillBePoped->id,
    		'quality_score' =>4
    	]);  
    	
    	AssignQueue::create([
    		'user_id' => $userWillNotBePoped->id,
    		'quality_score' =>4
    	]);  
    
    	
    	AssignQueue::create([
    		'user_id' => $userWithDiffrentTarget->id,
    		'quality_score' =>5
    	]);  

    	$this->assign->assignToQueuedUser();


    	$this->assertDatabaseMissing('assign_queues',[
    		'user_id' => $userWillBePoped->id,
    		'quality_score' => 4
    	]);
    	
    	$this->assertDatabaseHas('assign_queues',[
    		'user_id' => $userWillNotBePoped->id,
    		'quality_score' => 4
    	]);

    	$this->assertDatabaseHas('assign_queues',[
    		'user_id' => $userWithDiffrentTarget->id,
    		'quality_score' => 5
    	]);

    	$this->assertDatabaseHas('users',[
    		'id' => $userWillBePoped->id,
    		'total_leads' => 1,
    		'funds' => 9,
    	]);	

    	$this->assertDatabaseHas('users',[
    		'id' => $userWillNotBePoped->id,
    		'total_leads' => 0,
    		'funds' => 10,
    	]); 
    
    	$this->assertDatabaseHas('users',[
    		'id' => $userWithDiffrentTarget->id,
    		'total_leads' => 0,
    		'funds' => 10,
    	]);
    }

}
