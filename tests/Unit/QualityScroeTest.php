<?php namespace Tests\Unit;

use Mockery;
use App\User;
use App\Lead;
use Tests\TestCase;
use App\Service\AgeScore;
use App\Service\BaseScore;
use App\Service\CountryScore;
use App\Service\PaidUserScore;
use App\Service\VacationScore;
use App\Service\ValidInfoScore;
use App\Service\DayDurationScore;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class QualityScroeTest extends TestCase
{
    use RefreshDatabase;

	protected $qualityScore;
	/**
	* Setup the test environment.
	*
	* @return void
	*/
    protected function setUp(){
		parent::setUp();
    }
     /**
     *
     * @test
     */
    public function it_can_calculate_age_score(){
        $lead = factory(Lead::class)->create(['age' => '20']);
        $this->assertEquals(2, (new AgeScore(new BaseScore($lead)))->getScore());
    }     

    /**
     *
     * @test
     */
    public function it_can_calculate_valid_info_score(){

    	$lead = factory(Lead::class)->create(['first_name' => '%$@$']);
        $this->assertEquals(0, (new ValidInfoScore(new BaseScore($lead)))->getScore());
        
        $lead = factory(Lead::class)->create(['first_name' => 'x']);
        $this->assertEquals(0, (new ValidInfoScore(new BaseScore($lead)))->getScore());
        
        $lead = factory(Lead::class)->create(['email' => 'not_valid_email']);
        $this->assertEquals(0, (new ValidInfoScore(new BaseScore($lead)))->getScore());
        
        $lead = factory(Lead::class)->create(['phone' => 'asdasdasd']);
        $this->assertEquals(0, (new ValidInfoScore(new BaseScore($lead)))->getScore());

        $lead = factory(Lead::class)->create(['age' => '11']);
        $this->assertEquals(0, (new ValidInfoScore(new BaseScore($lead)))->getScore());

        $lead = factory(Lead::class)->create(['first_name' => 'valid name']);
    	$this->assertEquals(2, (new ValidInfoScore(new BaseScore($lead)))->getScore());

    }

    /**
     * @test
     */
    public function it_can_calculate_day_duration_score(){
    	$lead = factory(Lead::class)->create(['created_at' => '2018-07-27 13:00:00']);
    	$this->assertEquals(2, (new DayDurationScore(new BaseScore($lead)))->getScore());
    }
    /**
     * @test
     */
    public function it_can_calculate_vacation_score(){
        $lead = factory(Lead::class)->create(['created_at' => '2018-07-29 13:00:00']);
        $this->assertEquals(0, (new VacationScore(new BaseScore($lead)))->getScore());
    }   
    /**
     * @test
     */
    public function it_can_calculate_vacation_and_age_score(){
        $lead = factory(Lead::class)->create(['age'=>20,'created_at' => '2018-07-30 13:00:00']);
        $this->assertEquals(4, (new VacationScore(new AgeScore(new BaseScore($lead))))->getScore());
    }   

    /**
     * @test
     */
    public function it_can_calculate_paid_user_score(){
        $lead = factory(Lead::class)->create();

        $service = Mockery::mock(PaidUserScore::class,[new BaseScore($lead)])->makePartial();
        $service->shouldReceive('calculateScore')->andReturn(5);
    	$this->assertEquals(5, $service->getScore());
    }   
    
    /**
     * @test
     */
    public function it_can_calculate_country_score(){
        $user = factory(User::class)->create();
        $user->countries()->sync([1,2,3]);
        $lead = factory(Lead::class)->create(['country_id' => 1]);
        $leadWithLowScore = factory(Lead::class)->create(['country_id' => 4]);

        $this->assertEquals(2, (new CountryScore(new BaseScore($lead)))->getScore());
        $this->assertEquals(0, (new CountryScore(new BaseScore($leadWithLowScore)))->getScore());
        $this->assertDatabaseHas('leads',[
            'id' => $leadWithLowScore->id,
            'not_valid' => 1,
        ]);
        $this->assertDatabaseMissing('leads',[
            'id' => $lead->id,
            'not_valid' => 1,
        ]);

        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'country_score',
            'score' => 2
        ]);

        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $leadWithLowScore->id,
            'log' => 'country_score',
            'score' => 0
        ]);
    }   

    /**
     * @test
     */
    public function it_can_log_score(){

        $lead = factory(Lead::class)->create(['created_at' => '2018-07-27 13:00:00']);
        $this->assertEquals(2, (new DayDurationScore(new BaseScore($lead)))->getScore());
        
        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'day_duration_score',
            'score' => 2
        ]);
    }
}
