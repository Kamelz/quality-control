<?php

namespace Tests\Feature;
use Mail;
use Auth;
use App\User;
use App\Message;
use Tests\TestCase;
use App\Mail\AdminMessage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
	use RefreshDatabase;
    /**
     *
     * @test
     * @return void
     */
    public function only_admin_can_register_user()
    {

		$admin = factory(User::class)->create(['is_admin' => 1]);
		$reqgularUser  = factory(User::class)->create(['is_admin' => 0]);
		$userWillBeRegistered  = factory(User::class)->raw([
			'password' =>'123456',
			'password_confirmation' =>'123456'
		]);

		$this->actingAs($admin);

		$this->post('/users',$userWillBeRegistered+['countries' => [1,2,3]])
		->assertStatus(200);



		$this->assertDatabaseHas('users',[
			'name' => $userWillBeRegistered['name'],
			'email' => $userWillBeRegistered['email'],
			'phone' => $userWillBeRegistered['phone'],
		]);
		
		$userWillBeRegistered  = factory(User::class)->raw([
			'password' =>'123456',
			'password_confirmation' =>'123456'
		]);

		Auth::logout();
        $this->expectException(AuthenticationException::class);

		$this->post('/users',$userWillBeRegistered +['countries' => [1,2,3]])
		->assertStatus(302)
		->assertRedirect('/home');

		$this->assertDatabaseMissing('users',[
			'name' => $userWillBeRegistered['name'],
			'email' => $userWillBeRegistered['email'],
			'phone' => $userWillBeRegistered['phone'],
		]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function only_admin_can_update_users()
    {
		$admin = factory(User::class)->create(['is_admin' => true]);
		$reqgularUser  = factory(User::class)->create(['is_admin' => false]);
		$user  = factory(User::class)->create();
		
		$data = $user->getAttributes();
		$data['name'] = 'updated name';
		$data['email'] = 'updatedmail@test.com';
		$data['phone'] = '01122524554';
		$data['countries'] = [1,2,3];

		$this->actingAs($admin);
		$this->put("/users/{$user->id}",$data)
		->assertStatus(200);
	
		$this->assertDatabaseHas('users',[
			'name' => $data['name'],
			'email' => $data['email'],
			'phone' => $data['phone'],
		]);

		Auth::logout();
        $this->expectException(AuthenticationException::class);

		$data['name'] = 'updated name2';
		$data['email'] = 'updatedmail2@test.com';
		$data['phone'] = '011225245542';
		$data['countries'] = [1,2,3];



		$this->put("/users/{$user->id}",$data)
		->assertRedirect('/home');

		$this->assertDatabaseMissing('users',[
			'name' => $data['name'],
			'email' => $data['email'],
			'phone' => $data['phone'],
		]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function only_admin_can_get_users()
    {
       	$admin = factory(User::class)->create(['is_admin' => true]);
		$users  = factory(User::class,2)->create();

		$this->actingAs($admin);
		$response = $this->get("/users/get")
		->assertStatus(200);

		$this->assertSame($response->getOriginalContent()['data'][0]['email'],$users->toArray()[0]['email']);
		$this->assertSame($response->getOriginalContent()['data'][1]['email'],$users->toArray()[1]['email']);
		
		Auth::logout();
		$reqgularUser  = factory(User::class)->create(['is_admin' => false]);
		$this->actingAs($reqgularUser);
		
		$response = $this->get("/users/get")
		->assertStatus(302);
    }

     /**
     *
     * @test
     * @return void
     */
    public function admin_can_receive_message_from_a_user(){
   		Mail::fake();
		$admin = factory(User::class)->create(['is_admin' => true]);
		$reqgularUser  = factory(User::class)->create(['is_admin' => false]);
		$this->actingAs($reqgularUser);

		$data = [
			'message'=>'hello',
			'to' => $admin->id,
			'from' => $reqgularUser->id
		];

		$response = $this->post("/messages/{$admin->id}",$data)
			->assertStatus(200);

		$this->assertDatabaseHas('messages',[
			'message' => 'hello',
			'to' => $admin->id,
			'from' => $reqgularUser->id
		]);
 		Mail::assertSent(AdminMessage::class, function ($mail) use ($admin) {
            return $mail->hasTo($admin->email);
        });

    }

    /**
     *
     * @test
     * @return void
     */
    public function admin_can_send_message_to_a_user()
    {
    	Mail::fake();
		$admin = factory(User::class)->create(['is_admin' => true]);

		$reqgularUser  = factory(User::class)->create(['is_admin' => false]);
		$this->actingAs($admin);
		
		$data = [
			'message'=>'hello',
			'from' => $admin->id,
			'to' => $reqgularUser->id
		];

		$response = $this->post("/messages/{$reqgularUser->id}",$data)
		->assertStatus(200);
		
		$this->assertDatabaseHas('messages',[
			'message' => 'hello',
			'from' => $admin->id,
			'to' => $reqgularUser->id
		]);

		 Mail::assertSent(AdminMessage::class, function ($mail) use ($reqgularUser) {
            return $mail->hasTo($reqgularUser->email);
        });
    }

 
     /**
     *
     * @test
     * @return void
     */
    public function only_admin_can_choose_which_user_can_see_all_information()
    {
		$admin = factory(User::class)->create(['is_admin' => true]);
		$userCannotSeeAllInformation  = factory(User::class)->create(['can_see_all_information' => 0]);
		$reqgularUser  = factory(User::class)->create(['is_admin' => false]);
		$this->actingAs($admin);
		$data = [
		'status' => true
		];
		$this->post("/users/information-visibility/{$userCannotSeeAllInformation->id}",$data)
		->assertStatus(200);

		$this->assertDatabaseHas('users',[
			'id' => $userCannotSeeAllInformation->id,
			'can_see_all_information' => 1
		]);
		
		$user = User::whereId($userCannotSeeAllInformation->id)->first();
		$user->can_see_all_information = false;
		$user->save();
		
		$this->actingAs($reqgularUser);

		$this->post("/users/information-visibility/{$userCannotSeeAllInformation->id}",$data)
		->assertStatus(302);	
		$this->assertDatabaseMissing('users',[
			'id' => $userCannotSeeAllInformation->id,
			'can_see_all_information' => 1
		]);
    }
}
