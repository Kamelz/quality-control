<?php

namespace Tests\Feature;
use Mail;
use Auth;
use App\User;
use App\Lead;
use Carbon\Carbon;
use Tests\TestCase;
use App\AssignQueue;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeadTest extends TestCase
{
	use RefreshDatabase;

    /**
     *
     * @test
     * @return void
     */
    public function a_lead_can_register()
    {

       $lead = factory(Lead::class)->raw();
       $lead +=['country'=>'EG']; 
       $this->post('/api/leads', $lead)->assertStatus(302);

       $this->assertDatabaseHas('leads',[
        'first_name' => $lead['first_name'],
        'last_name' => $lead['last_name'],
        'email' => $lead['email'],
        'phone' => $lead['phone'],
        'age' => $lead['age'],
        'land_id' => $lead['land_id'],
       ]);
    }
    
    /**
     *
     * @test
     * @return void
     */
    public function a_lead_is_assigned_to_a_partner_after_registring()
    {


        $knownDate = Carbon::create(2018, 7, 30, 7); // create testing date
        Carbon::setTestNow($knownDate);

        $userWithHigherTarget = factory(User::class)->create([
            'funds' => 200,
            'action_price' => 10,
            'total_leads' => 0,
            'daily_leads' => 4,
            'quality_score_target_from' => 3,
            'quality_score_target_to' => 7,
        ]);
        
        $userWithLowerTarget = factory(User::class)->create([
            'funds' => 200,
            'action_price' => 10,
            'total_leads' => 0,
            'daily_leads' => 4,
            'quality_score_target_from' => 1,
            'quality_score_target_to' => 2,
        ]);

        $userWithTheRightTarget1 = factory(User::class)->create([
            'funds' => 200,
            'action_price' => 10,
            'total_leads' => 100,
            'daily_leads' => 4,
            'quality_score_target_from' => 8,
            'quality_score_target_to' => 10,
        ]);   
        $userWithTheRightTarget2 = factory(User::class)->create([
            'funds' => 200,
            'action_price' => 10,
            'total_leads' => 0,
            'daily_leads' => 4,
            'quality_score_target_from' => 8,
            'quality_score_target_to' => 10,
        ]);
        $lead = factory(Lead::class)->raw(['user_id' => null]); // monday, morning
        $lead +=['country'=>'EG']; 
        
        $this->post('/api/leads',$lead)
            ->assertStatus(200);

        $lead = Lead::first();

        $this->assertDatabaseHas('leads',[
            'id' => $lead->id,
            'user_id' => $userWithTheRightTarget1->id,
        ]);
        
        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'vacation_score',
            'score' => 2
        ]);   

        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'valid_info_score',
            'score' => 2
        ]);    
        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'age_score',
            'score' => 2
        ]);    

        $this->assertDatabaseHas('qualityscore_logs',[
            'lead_id' => $lead->id,
            'log' => 'day_duration_score',
            'score' => 2
        ]);

        $this->assertDatabaseHas('users',[
            'id' => $userWithTheRightTarget1->id,
            'funds' => 190,
            'total_leads' => 99,
        ]); 
        
        $this->assertDatabaseHas('users',[
            'id' => $userWithTheRightTarget2->id,
            'funds' => 200,
            'total_leads' => 0,
        ]); 

        $this->assertDatabaseHas('users',[
            'id' => $userWithHigherTarget->id,
            'funds' => 200,
            'total_leads' => 0,
        ]); 

        $this->assertDatabaseHas('users',[
            'id' => $userWithLowerTarget->id,
            'funds' => 200,
            'total_leads' => 0,
        ]); 


        $this->assertDatabaseHas('assign_queues',[
            'user_id' => $userWithTheRightTarget2->id,
            'quality_score' => 8,
            'rest_of_daily_leads' => 4,
        ]);

        $this->assertDatabaseHas('assign_queues',[
            'user_id' => $userWithTheRightTarget1->id,
            'quality_score' => 8,
            'rest_of_daily_leads' => 3,
        ]);

        $this->assertDatabaseMissing('assign_queues',[
            'user_id' => $userWithHigherTarget->id,
            'quality_score' => 8,
            'rest_of_daily_leads' => 4,
        ]);

        $this->assertDatabaseMissing('assign_queues',[
            'user_id' => $userWithLowerTarget->id,
            'quality_score' => 8,
            'rest_of_daily_leads' => 4,
        ]);
    }

    /**
     *
     * @test
     * @return void
     */
    public function only_admin_can_assign_lead_manually()
    {
        $admin = factory(User::class)->create(['is_admin' =>1]);
        
        $regularUser = factory(User::class)->create();
        
        $queueduser = factory(User::class)->create([
            'funds' => 10,
            'total_leads' => 0,
        ]);

        $lead = factory(Lead::class)->create(['user_id' => null]);
        $this->actingAs($admin);
        
        $this->post("/leads/register/{$lead->id}/{$queueduser->id}")
        ->assertStatus(200);
        $this->assertDatabaseHas('leads',[
            'id' => $lead->id,
            'user_id' => $queueduser->id
        ]);
        
        $this->assertDatabaseHas('users',[
            'id' => $queueduser->id,
            'funds' => 9,
            'total_leads' => 1,
        ]);

        $this->actingAs($regularUser);
        $queueduser = factory(User::class)->create([
            'funds' => 10,
            'total_leads' => 0,
        ]);
        $lead = factory(Lead::class)->create(['user_id' => null]);
        $this->post("/leads/register/{$lead->id}/{$queueduser->id}")
        ->assertStatus(302);


         $this->assertDatabaseMissing('leads',[
            'id' => $lead->id,
            'user_id' => $queueduser->id
        ]);
        
        $this->assertDatabaseMissing('users',[
            'id' => $queueduser->id,
            'funds' => 9,
            'total_leads' => 1,
        ]);

    }   
}
