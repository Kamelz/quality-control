<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned();
            
            $table->string('first_name');
            $table->string('last_name');
            $table->string('land_id');
            $table->string('website');
            $table->string('email');
            $table->string('phone');
            // $table->string('email')->unique();
            // $table->string('phone')->unique();
            
            $table->integer('age');
            $table->integer('quality_score')->default(1);
            $table->integer('not_valid')->default(0);
            $table->string('feedback')->default('No feedback yet');
            
            $table->foreign('user_id')
            ->references('id')->on('users');
            
            $table->foreign('country_id')
            ->references('id')->on('countries');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
