<?php

use App\User;
use App\Lead;
use App\Message;
use Illuminate\Database\Seeder;
class GenerateDummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      $this->call(CountrySeeder::class);
      $admin = User::where('email','admin@admin.com')->where('is_admin',true)->first();
      if(!$admin){
       // $this->call(UsersTableSeeder::class);
       $admin = factory(User::class)->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'can_see_all_information' => true,
            'phone' => 0,
            'is_admin' => true,
            'funds' => 100000000,
            'total_leads' => 100000000,
            'daily_leads' => 0,
            'action_price' => 0,
            'quality_score_target_from' => 0,
            'quality_score_target_to' => 0,
        ]);
        $this->command->info('Admin user seeded!');
      }
      $leads = factory(Lead::class,4)->create();
   
      foreach ($leads as $lead) {

     		$lead->user->countries()->sync([5,7,8]);
     		Message::create([
          'to' => $lead->user_id,
     			'from' => $admin->id,
     			'message' => 'hello',
     		]);
      }
    }
}
