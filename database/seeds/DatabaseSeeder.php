<?php

use App\User;
use App\Country;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $adminExists = User::where('email','admin@admin.com')->where('is_admin',true)->exists();
        if(!$adminExists){
        // $this->call(UsersTableSeeder::class);
            factory(User::class)->create([
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('123456'),
                'phone' => 0,
                'can_see_all_information' => true,
                'is_admin' => true,
                'funds' => 100000000,
                'total_leads' => 100000000,
                'daily_leads' => 0,
                'action_price' => 0,
                'quality_score_target_from' => 0,
                'quality_score_target_to' => 0,
            ]);
            $this->command->info('Admin user seeded!');
        }
        
        $this->call(CountrySeeder::class);
    
    }
}
