<?php 

use Illuminate\Database\Seeder;
use App\Country;
class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$countriesNotEmpty = Country::count();
		if($countriesNotEmpty === 0){
			$path = 'sql/countries.sql';
			// dd("../../".__file__."/sql/countries.sql");
			DB::unprepared(file_get_contents(base_path($path)));
		}
    }
}
