<?php
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => 'test name',
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->randomNumber(8),
        'funds' => 200,
        'total_leads' => 0,
        'daily_leads' => 20,
        'can_see_all_information' => false,
        'action_price' => $faker->randomDigit,
        'quality_score_target_from' => 5,
        'quality_score_target_to' => 10,
        'password' => Hash::make('123456'), // secret
        'remember_token' => str_random(10),
        'is_admin' => false
    ];
});