<?php

use App\Lead;
use App\User;
use Faker\Generator as Faker;
$factory->define(Lead::class, function (Faker $faker) {
    return [
    	'user_id' => function(){
    		return factory(User::class)->create()->id;
    	},
    	'country_id' => 1,
        'first_name' => 'fname',
        'last_name' => 'lname',
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->randomDigit,
        'age' => 20,
        'land_id' =>'ios_1503367',
        'website' =>'www.asdasdas.com',
        'quality_score' => 0,
        'not_valid' => 0,
    ];
});
