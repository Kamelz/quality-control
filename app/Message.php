<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Message extends Model
{
       /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'from',
        'to',
    ];
    
    public function user(){

    	return $this->hasOne(User::class,'id','from');
    }
}
