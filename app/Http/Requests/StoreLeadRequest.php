<?php namespace App\Http\Requests;

use Mail;
use App\User;
use App\Lead;
use App\Country;
use App\Mail\NotifyLead;
use App\Service\AgeScore;
use App\Service\BaseScore;
use App\Service\AssignLead;
use App\Service\CountryScore;
use App\Service\VacationScore;
use App\Service\ValidInfoScore;
use App\Service\DayDurationScore;
use Illuminate\Foundation\Http\FormRequest;
class StoreLeadRequest extends FormRequest
{
    protected $lead;
    protected $data;
    protected $qualityScore;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return [
        //     'first_name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
        //     'last_name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
        //     'email' => 'required|string|email|unique:leads',
        //     'phone' => 'required|numeric|unique:leads',
        //     'country' => 'required|exists:countries,code',
        //     'age' => 'required|integer',
        //     'land_id' => 'required',
        // ];

        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'country' => 'required',
            'age' => 'required',
            'land_id' => 'required',
            'click_id' => 'nullable',
            // 'website' => 'required',
        ];
    }

    public function registerLead(){
        $this->data = $this->validated();
        $country = Country::where('code',$this->data['country'])->first();
        $this->data +=['country_id' =>$country->id]; 
        $this->lead = Lead::create($this->data);

        return $this;
    }

    public function calculateQualityScore(){
        $this->qualityScore = (
            new VacationScore(
            new DayDurationScore(
            new AgeScore(
            new ValidInfoScore(
            new CountryScore(
            new BaseScore($this->lead)
            )))))
        )->getScore();

        $this->lead->quality_score =  $this->qualityScore;
        $this->lead->save();
        return $this;
    }

    public function assignToUser(){

        // check if the score in the assigne_queue table
        // then pop on the first user and assign lead to it

        // if not in the table
        // get all matched users
        // add them in the table
        // then pop on the first user and assign lead to it

        $assignLead = new AssignLead($this->lead);
        
        if($assignLead->isAssignQueueNotEmpty()){

            $assignLead->assignToQueuedUser(); 

        }else{

            $assignLead
            ->addMatchedUsersToQueue()
            ->assignToQueuedUser(); 
        }

        return $this;
    }

    public function mail(){

        Mail::to($this->lead)->send(new NotifyLead());

        return $this;
    }
}
