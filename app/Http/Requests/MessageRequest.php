<?php

namespace App\Http\Requests;

use Mail;
use App\User;
use App\Message;
use App\Mail\AdminMessage;
use Illuminate\Foundation\Http\FormRequest;

class MessageRequest extends FormRequest
{
    protected $to;
    protected $from;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'message' =>'required|string',
        ];
    }

    public function to(User $user){
      
        $this->to = $user;
        return $this;
    }

    public function from(User $user){
      
        $this->from = $user;
        return $this;
    }

    public function send(){
        $data = $this->validated();
       
        Message::create([
            'message' => $data['message'],
            'from' => $this->from->id,
            'to' => $this->to->id
        ]);

        return $this;   
    }

    public function mail(){
        $data = $this->validated();
        Mail::to($this->to)->send(new AdminMessage($data['message']));
        return $this;
    }
}
