<?php

namespace App\Http\Requests;
use Mail;
use App\User;
use App\Mail\NotifyUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method()){
            case 'POST':
                return [
                    'name' => 'required|string|max:255|regex:/^[a-zA-Z ]+$/',
                    'password' => 'required|string|confirmed',
                    'email' => 'required|string|email|unique:users',
                    'phone' => 'required|numeric|unique:users',
                    'funds' => 'required|integer|min:0',
                    'total_leads' => 'required|integer|min:0',
                    'countries' => 'required|array',
                    'daily_leads' => 'required|integer|min:0',
                    'action_price' => 'required|integer|min:0',
                    'quality_score_target_from' => 'required|integer|lte:quality_score_target_to|min:0',
                    'quality_score_target_to' => 'required|integer|max:10',
                ];
                break;

            case 'PUT':
                return [
                    'name' => 'string|max:255|regex:/^[a-zA-Z ]+$/',
                    'email' => 'string|email',
                    'phone' => 'numeric',
                    'countries' => 'array',
                    'funds' => 'integer|min:0',
                    'total_leads' => 'integer|min:0',
                    'daily_leads' => 'integer|min:0',
                    'action_price' => 'integer|min:0',
                    'quality_score_target_from' => 'integer|lte:quality_score_target_to|min:0',
                    'quality_score_target_to' => 'integer|max:10',
                ];
                break;
        }
    }

    public function registerUser(){
        $data = $this->validated();
        $password=$data['password'];
        $data['password'] =  Hash::make($data['password']);
        $user = User::create($data);
        $user->countries()->sync($data['countries']);
        Mail::to($user)->send(new NotifyUser($data['email'],$password));
        return $user;
    }
    
    public function updateUser(User $user){
        $data = $this->validated();
        // $data['password'] =  Hash::make($data['password']);
        $user->countries()->sync($data['countries']);
        return $user->update($data);
    }
}