<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Requests\MessageRequest;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::where('is_admin',false)->get();
        $usersMessages  = Message::where('to',auth()->user()->id)
        ->with('user')
        ->get()
        ->groupBy('from')
        ->flatMap(function ($items) {

        $messages = $items->pluck('message','created_at');
    
        return $items->map(function ($item,$key) use ($messages) {

            $item->message = $messages;

            return $item;

            });

        })
        ->unique('from');
        return view('admin.message')->with(['usersMessages'=>$usersMessages,'users'=>$users]);
    }

    public function markAsRead()
    {

        Message::where('to',auth()->user()->id)
        ->where('read',0)
        ->update(['read' => 1]);
        
        return ['success' => 200];
    }

    public function markUserMessageAsRead(User $user)
    {
    
        Message::where('from',$user->id)
        ->where('read',0)
        ->update(['read' => 1]);
        
        return ['success' => 200];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\MessageRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function send(User $user,MessageRequest $request)
    {
        
        $request
        ->to($user)
        ->from(auth()->user())
        ->send()
        ->mail();
        
        return ['success' =>200];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
