<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user()->with('leads');
        return view('user.index')->with(['user' => $user]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $users = User::where('is_admin',false)->with(['countries'=>function($query){
            $query->select('name');
        }])->get();
        return datatables()->of($users)->toJson();
    }

    public function export(Request $request){

       $data = $request->validate([
            'export-lead-from' => 'required',
            'export-lead-to' => 'required',
        ]);

        $from = $data['export-lead-from'];
        $to = $data['export-lead-to'];

        $users = User::where('is_admin',false)->with(['countries'=>function($query){
            $query->select('name');
        }])->get(); 

        return view('user.export')->with([
            'users' => $users
        ]);
    }

    public function getUserMessages(){
        $admin = User::where('is_admin',1)->first();
        $messages = Auth::user()->messages()->paginate(4);
        return view('user.message')->with(['admin'=>$admin ,'messages' => $messages]);
    }

    public function changeUserInformationVisibilityStatus(Request $request, User $user){
        $data =  $request->validate([
        'status' => 'required',
        ]);

        $user->can_see_all_information = (int)$data['status'];
        $user->save();
        return ['success' => 200];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request){
        
        $request->registerUser();
      
        return ['success' => 201];
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
       return view('user.show')->with(['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreUserRequest  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUserRequest $request, User $user)
    {
        $request->updateUser($user);
        
        return ['success' => 204];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
