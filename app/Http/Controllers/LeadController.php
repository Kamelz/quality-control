<?php

namespace App\Http\Controllers;

use DB;
use App\Lead;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLeadRequest;
class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {  
        if(auth()->user()->is_admin){

            $leads = Lead::with(['country'])->get();       
        }else{
            $leads = Lead::where('user_id',auth()->user()->id)->with(['country'])->get();
        }

        return datatables()->of($leads)->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lead.create');
    }

    public function showAssignForm()
    {
        $leads = Lead::where('user_id',null)
        ->where('not_valid',0)
        ->get();

        $users = User::where('is_admin',0)
        ->where('funds','>',0)
        ->get();

        return view('lead.assign')->with(
            [
                'leads' => $leads,
                'users' => $users,
            ]
        );
    }
    public function export(Request $request){

        $data = $request->validate([
            'export-lead-from' => 'required',
            'export-lead-to' => 'required',
        ]);

        $from = $data['export-lead-from'];
        $to = $data['export-lead-to'];

          if(auth()->user()->is_admin){
            $leads = Lead::with(['country'])->whereBetween('created_at',[$from,$to])->get();       
        }else{
            $leads = Lead::where('user_id',auth()->user()->id)->whereBetween('created_at',[$from,$to])->with(['country'])->get();
        }

        return view('lead.export')->with([
            'leads' => $leads
        ]);
    }

    public function assignLead(Request $request,Lead $lead,User $user){
        DB::transaction(function()use($lead,$user){
            $lead->user_id = $user->id;
                $lead->save();
                $user->funds -=$user->action_price;
                $user->total_leads --;
                $user->save();
            });
            return ['success' => 200];
        }
        /**
         * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLeadRequest $request)
    {   
        DB::transaction(function()use($request){
            $request->registerLead()
            ->calculateQualityScore()
            ->assignToUser();
            // ->mail();
        });

        return view('thankyou');
    }

    public function showQualityScoreLog(Lead $lead){
        // dd($lead->scoreLogs);
        return view('lead.score')->with(['scoreLogs' =>$lead->scoreLogs]);
    }

    public function feedback(Request $request){
        $data = $request->validate([
            'id' => 'required|exists:leads',
            'feedback' => 'required|string',
        ]);
        $lead = Lead::whereId($data['id'])->first();
        $lead->feedback = $data['feedback'];
        $lead->save();

        return ['success' => 200];
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }
}
