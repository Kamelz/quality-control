<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminMessage extends Mailable
{
    use Queueable, SerializesModels;

    protected $message;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {   
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.message')
        ->with([
            'attatchedMessage'=> $this->message
        ]);
    }
}
