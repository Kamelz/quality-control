<?php namespace App\Service;

use Carbon\Carbon;
use App\Traits\QualityScoreLogger;

class DayDurationScore implements QualityScore{
	use QualityScoreLogger;

	protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	public function calculateScore(){
		return $this->lead->created_at->hour >= 17 ? 0 : 2; // 17 => 5PM
	}
	
	public function getScore(){
		$this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}
	public function getlead(){
		return $this->lead;
	}
}

