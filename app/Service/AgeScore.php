<?php namespace App\Service;
use App\Traits\QualityScoreLogger;
class AgeScore implements QualityScore{
	use QualityScoreLogger;
	protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	public function calculateScore(){
		return $this->lead->age >= 18 ? 2 : 0;
	}
	public function getScore(){
		$this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}

	public function getlead(){
		return $this->lead;
	}
}

