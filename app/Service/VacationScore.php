<?php namespace App\Service;
use Carbon\Carbon;
use App\Traits\QualityScoreLogger;
class VacationScore implements QualityScore{
	use QualityScoreLogger;
	
	protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	public function calculateScore(){
		if($this->lead->created_at->format('l') === "Saturday" || $this->lead->created_at->format('l') === "Sunday"){
			return 0;
		}
		return 2;
	}
	
	public function getScore(){
		$this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}
	public function getlead(){
		return $this->lead;
	}
}

