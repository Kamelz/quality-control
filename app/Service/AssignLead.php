<?php namespace App\Service;

use DB;
use App\Lead;
use App\User;
use App\AssignQueue;
class AssignLead{

	protected $score;
	protected $users;

	public function __construct(Lead $lead){
	
		$this->lead = $lead;

		$this->score = $lead->quality_score;
	
	}

	/**
	* Get users that's target quality score range matches the $score
	*/
	private function getMatchedUsers(){
		return User::where('quality_score_target_from','<=',$this->score)
		->where('quality_score_target_to','>=',$this->score)
		->where('is_admin',0)
		->where('funds','>',0) 
		->get();
	}


	public function isAssignQueueNotEmpty(){

		return !! AssignQueue::where('quality_score',$this->score)->count();
	}
	
	public function addMatchedUsersToQueue(){

		$users = $this->getMatchedUsers();

		foreach ($users as $user) {
			AssignQueue::create([
	    		'user_id' => $user->id,
	    		'quality_score' =>$this->score,
	    		'rest_of_daily_leads' =>$user->daily_leads,
    		]);  
		}
		return $this;
	}

	public function assignToQueuedUser(){
		
		$queuedUser = AssignQueue::where('quality_score',$this->score)->orderBy('created_at', 'desc')->first();
		if(!$queuedUser){return $this;}

		DB::transaction(function() use($queuedUser){
			$user = User::whereId($queuedUser->user_id)->first();
			$user->funds -=$user->action_price;
			$user->total_leads--;
			$user->save();
			
			$this->lead->user_id = $user->id;
			$this->lead->save();
			$queuedUser->rest_of_daily_leads--;
			$queuedUser->save();

			if($queuedUser->rest_of_daily_leads === 0){
				$queuedUser->delete();
			}
		});

		return $this;
	}
}

