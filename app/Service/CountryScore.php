<?php namespace App\Service;
use App\CountryUser;
use App\Traits\QualityScoreLogger;
class CountryScore implements QualityScore{
	use QualityScoreLogger;
	
	protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	public function calculateScore(){
		$countries = CountryUser::groupBy('country_id')->get()->pluck('country_id')->toArray();
		if (in_array($this->lead->country_id, $countries)){
			return 2;
		}
		$this->lead->not_valid = 1;
		$this->lead->save();
		return 0;
	}
	
	public function getScore(){
		$this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}
	public function getlead(){
		return $this->lead;
	}
}

