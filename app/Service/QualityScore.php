<?php namespace App\Service;

interface QualityScore
{
   public function calculateScore();
   public function getScore();
}
