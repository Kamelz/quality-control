<?php namespace App\Service;

use App\User;
use App\Traits\QualityScoreLogger;
class ValidInfoScore implements QualityScore{
	use QualityScoreLogger;
	
  protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	
	public function calculateScore(){


        if(!preg_match("/^[a-zA-Z ]+$/",$this->lead->first_name)){
          return 0;
        }

        if(strlen($this->lead->first_name) < 2){
        	return 0;
        }
        
        if(!preg_match("/^[a-zA-Z ]+$/",$this->lead->last_name)){
        	return 0;
        }
        
      	if(!filter_var($this->lead->email, FILTER_VALIDATE_EMAIL)){
        	return 0;
        }
        
      	if(!is_numeric($this->lead->phone)){

        	return 0;
        }

        if(User::where('phone',$this->lead->phone)->count()){

          return 0;
        }   

      	if(strlen($this->lead->phone) >11 || strlen($this->lead->phone) < 8 ){

        	return 0;
        }    

        if(!is_numeric($this->lead->age)){

       		return 0;
        } 

        if($this->lead->age < 18){

       		return 0;
        }

		return 2;
	}

	public function getScore(){
    $this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}

	public function getlead(){
		return $this->lead;
	}
}

