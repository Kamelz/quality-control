<?php namespace App\Service;
use App\Lead;
class BaseScore implements QualityScore{
	public $lead;
	
	public function __construct(Lead $lead){
		$this->lead = $lead;
	}
		
	public function calculateScore(){
		return 0;
	}
	public function getScore(){
		return $this->calculateScore();
	}
	
	public function getlead(){
		return $this->lead;
	}
}

