<?php namespace App\Service;
use App\Traits\QualityScoreLogger;
class PaidUserScore implements QualityScore{
	use QualityScoreLogger;
	
	protected $lead;
	protected $qualityScore;

	public function __construct(QualityScore $qualityScore){
		$this->qualityScore = $qualityScore;
		$this->lead = $this->qualityScore->getLead();
	}
	public function calculateScore(){
		// request paid users from secure.forex 
		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', 'https://secure.forexsignaltrade.com/users/paid');
		return in_array($this->lead->email, json_decode($res->getBody())) === true ? 5 : 0;
	}
	
	public function getScore(){
		$this->log();
		return $this->calculateScore() + $this->qualityScore->getScore();	
	}
	public function getlead(){
		return $this->lead;
	}
}

