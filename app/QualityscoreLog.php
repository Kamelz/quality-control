<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QualityscoreLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lead_id',
        'log',
        'score'
    ];
}
