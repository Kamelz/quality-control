<?php


function notAdmin(){
	if(\Auth::check()){
		return !\Auth::user()->is_admin;
	}
	return false;
}

function getUserMessagesCount(){
	if(\Auth::check()){
		return \App\Message::where('to',auth()->user()->id)->where('read',0)->count();
	}
	return 0;	
}