<?php

namespace App;

use App\Country;
use App\Message;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'funds',
        'total_leads',
        'daily_leads',
        'action_price',
        'quality_score_target_from',
        'quality_score_target_to',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function countries(){
        
        return $this->belongsToMany(Country::class);
    }

    public function messages(){
        
        return $this->hasMany(Message::class,'to');
    }
}
