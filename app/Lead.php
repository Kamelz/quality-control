<?php

namespace App;

use App\User;
use App\Country;
use App\QualityscoreLog;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'country_id',
        'email',
        'phone',
        'age',
        'not_valid',
        'feedback',
        'land_id',
        'website',
    ];

    public function user(){

        return $this->belongsTo(User::class);        
    }
    
    public function country(){
        
        return $this->belongsTo(Country::class);       
    }

    public function scoreLogs(){
        return $this->hasMany(QualityscoreLog::class);
    }
}
