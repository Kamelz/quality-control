<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignQueue extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quality_score',
        'user_id',
        'rest_of_daily_leads',
    ];
}
