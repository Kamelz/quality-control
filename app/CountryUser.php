<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryUser extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'country_user';
}
