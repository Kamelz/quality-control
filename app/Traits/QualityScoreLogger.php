<?php namespace App\Traits;

use App\QualityscoreLog;
use Illuminate\Support\Str;

trait QualityScoreLogger{

	public function log(){
		$pos = strrpos(get_class($this), '\\');
		$className = substr(get_class($this), $pos + 1);
		QualityscoreLog::create([
			'lead_id' => $this->lead->id,
			'log' => Str::snake($className),
			'score'=> $this->calculateScore()
		]);
	}
}