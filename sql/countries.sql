

INSERT INTO `countries` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', NULL, NULL),
(2, 'AX', 'Åland Islands', NULL, NULL),
(3, 'AL', 'Albania', NULL, NULL),
(4, 'DZ', 'Algeria', NULL, NULL),
(5, 'AS', 'American Samoa', NULL, NULL),
(6, 'AD', 'Andorra', NULL, NULL),
(7, 'AO', 'Angola', NULL, NULL),
(8, 'AI', 'Anguilla', NULL, NULL),
(9, 'AQ', 'Antarctica', NULL, NULL),
(10, 'AG', 'Antigua & Barbuda', NULL, NULL),
(11, 'AR', 'Argentina', NULL, NULL),
(12, 'AM', 'Armenia', NULL, NULL),
(13, 'AW', 'Aruba', NULL, NULL),
(14, 'AC', 'Ascension Island', NULL, NULL),
(15, 'AU', 'Australia', NULL, NULL),
(16, 'AT', 'Austria', NULL, NULL),
(17, 'AZ', 'Azerbaijan', NULL, NULL),
(18, 'BS', 'Bahamas', NULL, NULL),
(19, 'BH', 'Bahrain', NULL, NULL),
(20, 'BD', 'Bangladesh', NULL, NULL),
(21, 'BB', 'Barbados', NULL, NULL),
(22, 'BY', 'Belarus', NULL, NULL),
(23, 'BE', 'Belgium', NULL, NULL),
(24, 'BZ', 'Belize', NULL, NULL),
(25, 'BJ', 'Benin', NULL, NULL),
(26, 'BM', 'Bermuda', NULL, NULL),
(27, 'BT', 'Bhutan', NULL, NULL),
(28, 'BO', 'Bolivia', NULL, NULL),
(29, 'BA', 'Bosnia & Herzegovina', NULL, NULL),
(30, 'BW', 'Botswana', NULL, NULL),
(31, 'BR', 'Brazil', NULL, NULL),
(32, 'IO', 'British Indian Ocean Territory', NULL, NULL),
(33, 'VG', 'British Virgin Islands', NULL, NULL),
(34, 'BN', 'Brunei', NULL, NULL),
(35, 'BG', 'Bulgaria', NULL, NULL),
(36, 'BF', 'Burkina Faso', NULL, NULL),
(37, 'BI', 'Burundi', NULL, NULL),
(38, 'KH', 'Cambodia', NULL, NULL),
(39, 'CM', 'Cameroon', NULL, NULL),
(40, 'CA', 'Canada', NULL, NULL),
(41, 'IC', 'Canary Islands', NULL, NULL),
(42, 'CV', 'Cape Verde', NULL, NULL),
(43, 'BQ', 'Caribbean Netherlands', NULL, NULL),
(44, 'KY', 'Cayman Islands', NULL, NULL),
(45, 'CF', 'Central African Republic', NULL, NULL),
(46, 'EA', 'Ceuta & Melilla', NULL, NULL),
(47, 'TD', 'Chad', NULL, NULL),
(48, 'CL', 'Chile', NULL, NULL),
(49, 'CN', 'China', NULL, NULL),
(50, 'CX', 'Christmas Island', NULL, NULL),
(101, 'CC', 'Cocos (Keeling) Islands', NULL, NULL),
(102, 'CO', 'Colombia', NULL, NULL),
(103, 'KM', 'Comoros', NULL, NULL),
(104, 'CG', 'Congo - Brazzaville', NULL, NULL),
(105, 'CD', 'Congo - Kinshasa', NULL, NULL),
(106, 'CK', 'Cook Islands', NULL, NULL),
(107, 'CR', 'Costa Rica', NULL, NULL),
(108, 'CI', 'Côte d’Ivoire', NULL, NULL),
(109, 'HR', 'Croatia', NULL, NULL),
(110, 'CU', 'Cuba', NULL, NULL),
(111, 'CW', 'Curaçao', NULL, NULL),
(112, 'CY', 'Cyprus', NULL, NULL),
(113, 'CZ', 'Czechia', NULL, NULL),
(114, 'DK', 'Denmark', NULL, NULL),
(115, 'DG', 'Diego Garcia', NULL, NULL),
(116, 'DJ', 'Djibouti', NULL, NULL),
(117, 'DM', 'Dominica', NULL, NULL),
(118, 'DO', 'Dominican Republic', NULL, NULL),
(119, 'EC', 'Ecuador', NULL, NULL),
(120, 'EG', 'Egypt', NULL, NULL),
(121, 'SV', 'El Salvador', NULL, NULL),
(122, 'GQ', 'Equatorial Guinea', NULL, NULL),
(123, 'ER', 'Eritrea', NULL, NULL),
(124, 'EE', 'Estonia', NULL, NULL),
(125, 'ET', 'Ethiopia', NULL, NULL),
(126, 'EZ', 'Eurozone', NULL, NULL),
(127, 'FK', 'Falkland Islands', NULL, NULL),
(128, 'FO', 'Faroe Islands', NULL, NULL),
(129, 'FJ', 'Fiji', NULL, NULL),
(130, 'FI', 'Finland', NULL, NULL),
(131, 'FR', 'France', NULL, NULL),
(132, 'GF', 'French Guiana', NULL, NULL),
(133, 'PF', 'French Polynesia', NULL, NULL),
(134, 'TF', 'French Southern Territories', NULL, NULL),
(135, 'GA', 'Gabon', NULL, NULL),
(136, 'GM', 'Gambia', NULL, NULL),
(137, 'GE', 'Georgia', NULL, NULL),
(138, 'DE', 'Germany', NULL, NULL),
(139, 'GH', 'Ghana', NULL, NULL),
(140, 'GI', 'Gibraltar', NULL, NULL),
(141, 'GR', 'Greece', NULL, NULL),
(142, 'GL', 'Greenland', NULL, NULL),
(143, 'GD', 'Grenada', NULL, NULL),
(144, 'GP', 'Guadeloupe', NULL, NULL),
(145, 'GU', 'Guam', NULL, NULL),
(146, 'GT', 'Guatemala', NULL, NULL),
(147, 'GG', 'Guernsey', NULL, NULL),
(148, 'GN', 'Guinea', NULL, NULL),
(149, 'GW', 'Guinea-Bissau', NULL, NULL),
(150, 'GY', 'Guyana', NULL, NULL),
(151, 'HT', 'Haiti', NULL, NULL),
(152, 'HN', 'Honduras', NULL, NULL),
(153, 'HK', 'Hong Kong SAR China', NULL, NULL),
(154, 'HU', 'Hungary', NULL, NULL),
(155, 'IS', 'Iceland', NULL, NULL),
(156, 'IN', 'India', NULL, NULL),
(157, 'code', 'Indonesia', NULL, NULL),
(158, 'IR', 'Iran', NULL, NULL),
(159, 'IQ', 'Iraq', NULL, NULL),
(160, 'IE', 'Ireland', NULL, NULL),
(161, 'IM', 'Isle of Man', NULL, NULL),
(162, 'IL', 'Israel', NULL, NULL),
(163, 'IT', 'Italy', NULL, NULL),
(164, 'JM', 'Jamaica', NULL, NULL),
(165, 'JP', 'Japan', NULL, NULL),
(166, 'JE', 'Jersey', NULL, NULL),
(167, 'JO', 'Jordan', NULL, NULL),
(168, 'KZ', 'Kazakhstan', NULL, NULL),
(169, 'KE', 'Kenya', NULL, NULL),
(170, 'KI', 'Kiribati', NULL, NULL),
(171, 'XK', 'Kosovo', NULL, NULL),
(172, 'KW', 'Kuwait', NULL, NULL),
(173, 'KG', 'Kyrgyzstan', NULL, NULL),
(174, 'LA', 'Laos', NULL, NULL),
(175, 'LV', 'Latvia', NULL, NULL),
(176, 'LB', 'Lebanon', NULL, NULL),
(177, 'LS', 'Lesotho', NULL, NULL),
(178, 'LR', 'Liberia', NULL, NULL),
(179, 'LY', 'Libya', NULL, NULL),
(180, 'LI', 'Liechtenstein', NULL, NULL),
(181, 'LT', 'Lithuania', NULL, NULL),
(182, 'LU', 'Luxembourg', NULL, NULL),
(183, 'MO', 'Macau SAR China', NULL, NULL),
(184, 'MK', 'Macedonia', NULL, NULL),
(185, 'MG', 'Madagascar', NULL, NULL),
(186, 'MW', 'Malawi', NULL, NULL),
(187, 'MY', 'Malaysia', NULL, NULL),
(188, 'MV', 'Maldives', NULL, NULL),
(189, 'ML', 'Mali', NULL, NULL),
(190, 'MT', 'Malta', NULL, NULL),
(191, 'MH', 'Marshall Islands', NULL, NULL),
(192, 'MQ', 'Martinique', NULL, NULL),
(193, 'MR', 'Mauritania', NULL, NULL),
(194, 'MU', 'Mauritius', NULL, NULL),
(195, 'YT', 'Mayotte', NULL, NULL),
(196, 'MX', 'Mexico', NULL, NULL),
(197, 'FM', 'Micronesia', NULL, NULL),
(198, 'MD', 'Moldova', NULL, NULL),
(199, 'MC', 'Monaco', NULL, NULL),
(200, 'MN', 'Mongolia', NULL, NULL),
(201, 'ME', 'Montenegro', NULL, NULL),
(202, 'MS', 'Montserrat', NULL, NULL),
(203, 'MA', 'Morocco', NULL, NULL),
(204, 'MZ', 'Mozambique', NULL, NULL),
(205, 'MM', 'Myanmar (Burma)', NULL, NULL),
(206, 'NA', 'Namibia', NULL, NULL),
(207, 'NR', 'Nauru', NULL, NULL),
(208, 'NP', 'Nepal', NULL, NULL),
(209, 'NL', 'Netherlands', NULL, NULL),
(210, 'NC', 'New Caledonia', NULL, NULL),
(211, 'NZ', 'New Zealand', NULL, NULL),
(212, 'NI', 'Nicaragua', NULL, NULL),
(213, 'NE', 'Niger', NULL, NULL),
(214, 'NG', 'Nigeria', NULL, NULL),
(215, 'NU', 'Niue', NULL, NULL),
(216, 'NF', 'Norfolk Island', NULL, NULL),
(217, 'KP', 'North Korea', NULL, NULL),
(218, 'MP', 'Northern Mariana Islands', NULL, NULL),
(219, 'NO', 'Norway', NULL, NULL),
(220, 'OM', 'Oman', NULL, NULL),
(221, 'PK', 'Pakistan', NULL, NULL),
(222, 'PW', 'Palau', NULL, NULL),
(223, 'PS', 'Palestinian Territories', NULL, NULL),
(224, 'PA', 'Panama', NULL, NULL),
(225, 'PG', 'Papua New Guinea', NULL, NULL),
(226, 'PY', 'Paraguay', NULL, NULL),
(227, 'PE', 'Peru', NULL, NULL),
(228, 'PH', 'Philippines', NULL, NULL),
(229, 'PN', 'Pitcairn Islands', NULL, NULL),
(230, 'PL', 'Poland', NULL, NULL),
(231, 'PT', 'Portugal', NULL, NULL),
(232, 'PR', 'Puerto Rico', NULL, NULL),
(233, 'QA', 'Qatar', NULL, NULL),
(234, 'RE', 'Réunion', NULL, NULL),
(235, 'RO', 'Romania', NULL, NULL),
(236, 'RU', 'Russia', NULL, NULL),
(237, 'RW', 'Rwanda', NULL, NULL),
(238, 'WS', 'Samoa', NULL, NULL),
(239, 'SM', 'San Marino', NULL, NULL),
(240, 'ST', 'São Tomé & Príncipe', NULL, NULL),
(241, 'SA', 'Saudi Arabia', NULL, NULL),
(242, 'SN', 'Senegal', NULL, NULL),
(243, 'RS', 'Serbia', NULL, NULL),
(244, 'SC', 'Seychelles', NULL, NULL),
(245, 'SL', 'Sierra Leone', NULL, NULL),
(246, 'SG', 'Singapore', NULL, NULL),
(247, 'SX', 'Sint Maarten', NULL, NULL),
(248, 'SK', 'Slovakia', NULL, NULL),
(249, 'SI', 'Slovenia', NULL, NULL),
(250, 'SB', 'Solomon Islands', NULL, NULL),
(251, 'SO', 'Somalia', NULL, NULL),
(252, 'ZA', 'South Africa', NULL, NULL),
(253, 'GS', 'South Georgia & South Sandwich Islands', NULL, NULL),
(254, 'KR', 'South Korea', NULL, NULL),
(255, 'SS', 'South Sudan', NULL, NULL),
(256, 'ES', 'Spain', NULL, NULL),
(257, 'LK', 'Sri Lanka', NULL, NULL),
(258, 'BL', 'St. Barthélemy', NULL, NULL),
(259, 'SH', 'St. Helena', NULL, NULL),
(260, 'KN', 'St. Kitts & Nevis', NULL, NULL),
(261, 'LC', 'St. Lucia', NULL, NULL),
(262, 'MF', 'St. Martin', NULL, NULL),
(263, 'PM', 'St. Pierre & Miquelon', NULL, NULL),
(264, 'VC', 'St. Vincent & Grenadines', NULL, NULL),
(265, 'SD', 'Sudan', NULL, NULL),
(266, 'SR', 'Suriname', NULL, NULL),
(267, 'SJ', 'Svalbard & Jan Mayen', NULL, NULL),
(268, 'SZ', 'Swaziland', NULL, NULL),
(269, 'SE', 'Sweden', NULL, NULL),
(270, 'CH', 'Switzerland', NULL, NULL),
(271, 'SY', 'Syria', NULL, NULL),
(272, 'TW', 'Taiwan', NULL, NULL),
(273, 'TJ', 'Tajikistan', NULL, NULL),
(274, 'TZ', 'Tanzania', NULL, NULL),
(275, 'TH', 'Thailand', NULL, NULL),
(276, 'TL', 'Timor-Leste', NULL, NULL),
(277, 'TG', 'Togo', NULL, NULL),
(278, 'TK', 'Tokelau', NULL, NULL),
(279, 'TO', 'Tonga', NULL, NULL),
(280, 'TT', 'Trincodead & Tobago', NULL, NULL),
(281, 'TA', 'Tristan da Cunha', NULL, NULL),
(282, 'TN', 'Tunisia', NULL, NULL),
(283, 'TR', 'Turkey', NULL, NULL),
(284, 'TM', 'Turkmenistan', NULL, NULL),
(285, 'TC', 'Turks & Caicos Islands', NULL, NULL),
(286, 'TV', 'Tuvalu', NULL, NULL),
(287, 'UM', 'U.S. Outlying Islands', NULL, NULL),
(288, 'VI', 'U.S. Virgin Islands', NULL, NULL),
(289, 'UG', 'Uganda', NULL, NULL),
(290, 'UA', 'Ukraine', NULL, NULL),
(291, 'AE', 'United Arab Emirates', NULL, NULL),
(292, 'GB', 'United Kingdom', NULL, NULL),
(293, 'UN', 'United Nations', NULL, NULL),
(294, 'US', 'United States', NULL, NULL),
(295, 'UY', 'Uruguay', NULL, NULL),
(296, 'UZ', 'Uzbekistan', NULL, NULL),
(297, 'VU', 'Vanuatu', NULL, NULL),
(298, 'VA', 'Vatican City', NULL, NULL),
(299, 'VE', 'Venezuela', NULL, NULL),
(300, 'VN', 'Vietnam', NULL, NULL),
(301, 'WF', 'Wallis & Futuna', NULL, NULL),
(302, 'EH', 'Western Sahara', NULL, NULL),
(303, 'YE', 'Yemen', NULL, NULL),
(304, 'ZM', 'Zambia', NULL, NULL),
(305, 'ZW', 'Zimbabwe', NULL, NULL);
