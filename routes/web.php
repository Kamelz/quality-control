<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
	return view('thankyou');
    return redirect('/home');
});


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::middleware(['auth'])->group(function () {
	
	Route::post('/mark-as-read', 'MessageController@markAsRead');	

	Route::post('/messages/{user}', 'MessageController@send');	
	Route::get('/users/messages', 'UserController@getUserMessages')->name('users.message');
	Route::post('/leads/get', 'LeadController@get')->name('lead.get');
	Route::post('/leads/feedback', 'LeadController@feedback');
	Route::post('/leads/export', 'LeadController@export');
	Route::post('/users/export', 'UserController@export');
	
	Route::middleware(['admin'])->group(function () {
		Route::get('/lead-score/{lead}', 'LeadController@showQualityScoreLog');
		
		Route::post('/mark-user-message-as-read/{user}', 'MessageController@markUserMessageAsRead');	

		Route::post('/leads/register/{lead}/{user}', 'LeadController@assignLead');
		Route::get('/leads/assign', 'LeadController@showAssignForm');
		Route::get('/users/get', 'UserController@get')->name('users.get');
		Route::post('/users/information-visibility/{user}', 'UserController@changeUserInformationVisibilityStatus');
		Route::get('/messages', 'MessageController@index')->name('message');

		Route::resource('users', 'UserController');
	});
});
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );

