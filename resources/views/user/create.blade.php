@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="alert alert-success" style="display:none" id="alert" role="alert">
        </div>
        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>
        <div class="col-md-12">
            <form class="form"  action="{{route('users.create')}}">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" value="">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" value="" id="email">
                </div>
                <div class="form-group">
                    <label for="password">{{ __('Password') }}</label>
                    
                    <input id="password" type="password" class="form-control" name="password" required>
                    
                </div>
                <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                    
                </div>  

                <div class="form-group">
                    <label for="countries">Countries</label>
                    
                    <select id="countries" class="form-control" multiple>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>
       

                <div class="form-group">
                    <label for="name">Phone:</label>
                    <input type="text" class="form-control" value="" id="phone">
                </div>
                
                <div class="form-group">
                    <label for="funds">Funds:</label>
                    <input type="number" class="form-control" value="" id="funds">
                </div>
                <div class="form-group">
                    <label for="total_leads">Total Leads:</label>
                    <input type="number" class="form-control" value="" id="total_leads">
                </div>
                
                <div class="form-group">
                    <label for="daily_leads">Daily Leads:</label>
                    <input type="number" class="form-control" value="" id="daily_leads">
                </div>
                
                <div class="form-group">
                    <label for="action_price">Action Price:</label>
                    <input type="number" class="form-control" value="" id="action_price">
                </div>
                <div class="form-group">
                    <label for="quality_score_target_from">Targeted Quality Score From:</label>
                    <input type="text" class="form-control" value="" id="quality_score_target_from">
                </div>
                
                <div class="form-group">
                    <label for="quality_score_target_to">Targeted Quality Score To:</label>
                    <input type="text" class="form-control" value="" id="quality_score_target_to">
                </div>
                <button type="button" id="create-user" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {

     $('#countries').select2();

    $("#alert").css('display', 'none');
    $("#create-user").click(function(e) {
        var id = $("#user_id").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var funds = $("#funds").val();
        var countries = $("#countries").val();
        var total_leads = $("#total_leads").val();
        var daily_leads = $("#daily_leads").val();
        var daily_leads = $("#daily_leads").val();
        var action_price = $("#action_price").val();
        var password = $("#password").val();
        var password_confirmation = $("#password_confirmation").val();
        var quality_score_target_from = $("#quality_score_target_from").val();
        var quality_score_target_to = $("#quality_score_target_to").val();

        var data = {
            name: name,
            email: email,
            phone: phone,
            funds: funds,
            total_leads: total_leads,
            daily_leads: daily_leads,
            action_price: action_price,
            password: password,
            countries: countries,
            password_confirmation: password_confirmation,
            quality_score_target_from: quality_score_target_from,
            quality_score_target_to: quality_score_target_to,
            "_token": "{{ csrf_token() }}",
        };
        $.ajax({
            url: '/users',
            type: 'POST',
            data: data,
            success: function(data) {
                 $("#error").hide();
                if (data.success === 201) {
                    
                    $("#alert").text("User created successfully!.");
                } else {
                    
                    $("#alert").text("Error while creating user!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                errorsHtml = "";
                $("#alert").hide();
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);
                window.animateToTop("error");
                $("#error").show();
            }
        });
    });
});
</script>
@endsection