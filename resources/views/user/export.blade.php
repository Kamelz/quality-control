
<table class="table">
<thead>
<tr>
    <th>Id</th>
    <th>Name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Targeted Countries</th>
    <th>Funds</th>
    <th>Daily leads</th>
    <th>Total leads</th>
    <th>Action Price (CPL-CPA)</th>
    <th>Target Quality Score From</th>
    <th>Target Quality Score To</th>
    <th>Created At</th>
</tr>
</thead>
<tbody>
    @foreach($users as $user)   
    <tr>
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->phone}}</td>
    <td>
    @foreach($user->countries as $country)
        <p>{{$country->name}}</p>
    @endforeach
    </td>
    <td>{{$user->funds}}</td>
    <td>{{$user->daily_leads}}</td>
    <td>{{$user->total_leads}}</td>
    <td>{{$user->action_price}}</td>
    <td>{{$user->quality_score_target_from}}</td>
    <td>{{$user->quality_score_target_to}}</td>
    <td>{{$user->created_at}}</td>

    </tr>
    @endforeach
</tbody>
</table>



<script type="text/javascript">
  (function(){
          window.print();
  })();
</script>