@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                @if(auth()->user()->is_admin)
                    @include('admin.users-table')
                @endif
                    @include('lead.lead-table')
            
            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
