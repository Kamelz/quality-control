@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Messages From The System</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($messages as $message)
                            <tr>
                                <td>{{$message->id}}</td>
                                <td>{{$message->created_at->diffForHumans()}}</td>
                                <td>{{$message->message}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $messages->links() }}
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        
        <div class="alert alert-success" style="display:none" id="alert" role="alert">
        </div>
        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Send a Message</div>
                <div class="card-body">
                    <form class="form">
                        <input type="hidden" id='user' value="{{$admin->id}}" />
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea type="message" class="form-control" placeholder="Write a message.." id="message"></textarea>
                        </div>
                        <button type="button" id="user-send-message" class="btn btn-default">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
    
$(document).ready(function() {

    $("#alert").css('display', 'none');
    $("#user-send-message").click(function(e) {
        var userId = $("#user").val();
        var message = $("#message").val();

        var data = {
            to: userId,
            message: message,
            "_token": "{{ csrf_token() }}",
        };
        $.ajax({
            url: '/messages/'+userId,
            type: 'POST',
            data: data,
            success: function(data) {
                $("#error").hide();
                if (data.success === 200) {
                    $("#alert").text("Message sent successfully!.");
                } else {
                    $("#alert").text("Error while sending the message!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                $("#alert").hide();
                errorsHtml = "";
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);
                window.animateToTop("error");
                $("#error").show();
            }
        });
    });
});
</script>

@endsection
