@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        

        <div class="alert alert-success" style="display:none" id="alert" role="alert">

        </div>

        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>
            <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">Allow partner to see all information:</label>
                <input type="checkbox" id="can-see-all-information" @if($user->can_see_all_information) checked @endif aria-label="Checkbox for following text input">
            </div>
        </div>
    </div>
        <div class="col-md-12">
            <form class="form"  action="{{route('users.update',$user->id)}}">
                <input type="hidden" value="{{$user->id}}" id="user_id">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" value="{{$user->email}}" id="email">
                </div>

                <div class="form-group">
                    <label for="countries">Countries</label>
                        
                       

                    <select id="countries" class="form-control" multiple>
                        @foreach($countries as $country)
                    
                            @if(in_array($country->name,$user->countries()->pluck('name')->toArray()))
                                <option selected value="{{$country->id}}">{{$country->name}}</option>
                            @else
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Phone:</label>
                    <input type="text" class="form-control" value="{{$user->phone}}" id="phone">
                </div>
                <div class="form-group">
                    <label for="funds">Funds:</label>
                    <input type="number" class="form-control" value="{{$user->funds}}" id="funds">
                </div>
                <div class="form-group">
                    <label for="total_leads">Total Leads:</label>
                    <input type="number" class="form-control" value="{{$user->total_leads}}" id="total_leads">
                </div>
                
                <div class="form-group">
                    <label for="daily_leads">Daily Leads:</label>
                    <input type="number" class="form-control" value="{{$user->daily_leads}}" id="daily_leads">
                </div>
                
                <div class="form-group">
                    <label for="action_price">Action Price:</label>
                    <input type="number" class="form-control" value="{{$user->action_price}}" id="action_price">
                </div>

                <div class="form-group">
                    <label for="quality_score_target_from">Targeted Quality Score From:</label>
                    <input type="text" class="form-control" value="{{$user->quality_score_target_from}}" id="quality_score_target_from">
                </div>

                 <div class="form-group">
                    <label for="quality_score_target_to">Targeted Quality Score To:</label>
                    <input type="text" class="form-control" value="{{$user->quality_score_target_to}}" id="quality_score_target_to">
                </div>


                <button type="button" id="update-user" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

<script sync>
    $(document).ready(function(){

        $('#countries').select2();
        $("#alert").css('display','none');
        $("#can-see-all-information").change(function(e){
            var data = {
                status:this.checked===true?1:0,
                "_token": "{{ csrf_token() }}",    
            };
            var id = $("#user_id").val();
            var successMessage = this.checked === true? "User can see all information.":"User can't see all information.";
            $.ajax({
                url: '/users/information-visibility/'+ id,
                type: 'POST',
                data: data,
                success: function(data) {
                    if(data.success === 200){
                        $("#alert").text(successMessage);
                    }else{
                        $("#alert").text("Error while updating user!.");
                    }
                    window.animateToTop("alert");

                    $("#alert").show();
                },
                error:function(data){

                    errorsHtml ="";
                    $.each( data.responseJSON.errors, function( key, value ) {

                        errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                    });
                     $("#error").html(errorsHtml);
                    window.animateToTop("error");
                      $("#error").show();
                }
            });
        });

        $("#update-user").click(function(e){
        var id = $("#user_id").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var funds = $("#funds").val();
        var countries = $("#countries").val();  
        var total_leads = $("#total_leads").val();
        var daily_leads = $("#daily_leads").val();
        var daily_leads = $("#daily_leads").val();
        var action_price = $("#action_price").val();
        var quality_score_target_from = $("#quality_score_target_from").val();
        var quality_score_target_to = $("#quality_score_target_to").val();
            var data = {
                name:name,
                email:email,
                phone:phone,
                funds:funds,
                countries:countries,
                total_leads:total_leads,
                daily_leads:daily_leads,
                action_price:action_price,
                quality_score_target_from:quality_score_target_from,
                quality_score_target_to:quality_score_target_to,
                "_token": "{{ csrf_token() }}",    
            };

            $.ajax({
                url: '/users/'+ id,
                type: 'PUT',
                data: data,
                success: function(data) {
                    if(data.success === 204){
                        $("#alert").text("User updated successfully!.");

                    }else{
                        $("#alert").text("Error while updating user!.");
                    }
                    window.animateToTop("alert");

                    $("#alert").show();
                },
                error:function(data){

                    errorsHtml ="";
                    $.each( data.responseJSON.errors, function( key, value ) {

                        errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                    });
                     $("#error").html(errorsHtml);
                    window.animateToTop("error");
                      $("#error").show();
                }
            });
        }); 
    });
</script>
@endsection