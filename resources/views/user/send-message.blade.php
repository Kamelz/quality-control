@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="alert alert-success" style="display:none" id="alert" role="alert">
        </div>
        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>
        <div class="col-md-12">
            <form class="form">
                <input type="hidden" name="user_id" value="{{$admin->id}}" />
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea type="message" class="form-control" placeholder="Write a message.." id="message"></textarea>
                </div>
                <button type="button" id="send-message" class="btn btn-default">Send</button>
            </form>
        </div>
    </div>
</div>
<script>
    
$(document).ready(function() {

    $('#user').select2();
    $("#alert").css('display', 'none');
    $("#send-message").click(function(e) {
        var userId = $("#user").val();
        var message = $("#message").val();

        var data = {
            user_id: userId,
            message: message,
            "_token": "{{ csrf_token() }}",
        };
        $.ajax({
            url: '/messages/'+userId,
            type: 'POST',
            data: data,
            success: function(data) {
                $("#error").hide();
                if (data.success === 201) {
                    $("#alert").text("Message sent successfully!.");
                } else {
                    $("#alert").text("Error while sending the message!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                $("#alert").hide();
                errorsHtml = "";
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);
                window.animateToTop("error");
                $("#error").show();
            }
        });
    });
});
</script>
@endsection