
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Account Information</div>
            <div class="card-body">
               <p> <strong>Name:</strong>  {{auth()->user()->name}}</p> 
               <p> <strong>Email:</strong> {{auth()->user()->email}}</p> 
               <p> <strong>Phone:</strong> {{auth()->user()->phone}}</p> 
               <p> <strong>Date of registration:</strong> {{auth()->user()->created_at}} ({{auth()->user()->created_at->diffForHumans()}})</p> 
               <p> <strong>Total Leads:</strong> {{auth()->user()->total_leads}}</p> 
               <p> <strong>Countries:</strong> 
                @foreach(auth()->user()->countries as $country)
                
                    <span class="badge badge-success">{{$country->name}}</span>
                @endforeach
               </p> 

            @if(auth()->user()->can_see_all_information)
               <p> <strong>Daily Leads:</strong> {{auth()->user()->daily_leads}}</p> 
               <p> <strong>Funds:</strong> {{auth()->user()->funds}}</p> 
               <p> <strong>Action Price:</strong> {{auth()->user()->action_price}}</p> 
            @endif
            </div>
        </div>
    </div>
</div>