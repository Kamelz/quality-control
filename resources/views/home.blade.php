@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if(auth()->user()->is_admin)
    	@include('admin.lead-table')
    @else
    	@include('lead.lead-table')
    @endif
 
     @if(auth()->user()->is_admin)
     	<br>
        @include('admin.users-table')
    @endif
 	<br>
    @if(notAdmin())
    	@include('user.info')
    @endif
</div>
@endsection
