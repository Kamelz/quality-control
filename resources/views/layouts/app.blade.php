<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}" ></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/dataTables.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/pace-theme-minimal.css') }}" rel="stylesheet">

        <script src="{{ asset('js/jquery.dataTables.min.js') }}" defer></script>
        <script src="{{ asset('js/select2.min.js') }}" defer></script>
        <script src="{{ asset('js/script.js') }}" defer></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/pace.min.js') }}"></script>

        
    </head>
    <body>
        <style type="text/css">
            .colored{
                color:red!important;
            }

            #wake-up{
                font-size: 19px;
                background-color: #f36d6d;
                font-weight: bold;
                color: white;   
            }
        </style>
        <center>
            <div style='display: none' id="wake-up">
                Attention you are not active, you'll be logged out in one minute.
            </div>
        </center>
        @if(auth()->check())
            @if(notAdmin())
            <script type="text/javascript" sync>

                $(document).ready(function(){

                    $("#wake-up").hide();            
                    var alarmTimer = 1.74e+6; //29 minutes
        
                    var showLogOutAlarm = setTimeout(wakeUp, alarmTimer);
                    
                    var logOutAfter = 60000; //1 minute
                    var logout=null;

                    function wakeUp(){
                        var audio = new Audio('/audio/to-the-point.mp3');
                        audio.play();
                        $("#wake-up").show();
                        logout = setTimeout(logOut, logOutAfter);
                    }

                    function logOut(){
                        window.location.href="/logout";
                    }

                    $(document.body).click(function(){
                        clearTimeout(showLogOutAlarm);
                        showLogOutAlarm = setTimeout(wakeUp, alarmTimer);
                        $("#wake-up").hide();  
                    });
                });

            </script>
            @endif
        @endif
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img style='width: 47%;' src="{{asset('/images/LONG.gif')}}">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                            @if(auth()->check())
                         <li class="nav-item">
                                <a class="nav-link"href="/home">Dashboard</a>
                            </li>
                                <?php 

                                    $messagesCount = getUserMessagesCount(); 
                                    $class = $messagesCount > 0? 'colored':'';
                                ?>
                                @if(auth()->user()->is_admin)
                            
                                <li class="nav-item">
                                    <a class="nav-link"href="/users/create">New partner</a>
                                </li>    
                               <li class="nav-item">
                                    <a class="nav-link"href="/leads/assign">Assign lead</a>
                                </li>
                             
                                <li class="nav-item">
                                    <a class="{{ $class }} nav-link" href="/messages">Messages ( {{getUserMessagesCount()}} )</a>
                                </li>
                                @else
                                 <li class="nav-item">
                                    <a class="{{ $class }} nav-link" onclick="markUserMessagesAsRead()" href="/users/messages">Messages ( {{getUserMessagesCount()}} )</a>
                                </li>
                                @endif
                            @endif
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <!-- li class="nav-item">
                                <a class="nav-link" href="/leads/create">{{ __('Register') }}</a>
                            </li> -->
                            @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </body>
</html>