@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="alert alert-success" style="display:none" id="alert" role="alert">
        </div>
        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>

        <div class="col-md-12">
            <form class="form"  action="{{route('users.create')}}">
                <div class="form-group">
                    <label for="name">Select Partner:</label>
                    <select class="form-control" id="user">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Select Lead:</label>
                    <select class="form-control" id="lead">
                        @foreach($leads as $lead)
                            <option value="{{$lead->id}}">{{$lead->first_name}} {{$lead->last_name}} ({{$lead->quality_score}}) </option>
                        @endforeach
                    </select>
                </div>

                <button type="button" id="assign-lead" class="btn btn-default">Assign</button>
            </form>
        </div>
    </div>
</div>
<script>
    
$(document).ready(function() {

    $('#user').select2();
    $("#alert").css('display', 'none');
    $("#assign-lead").click(function(e) {
        var userId = $("#user").val();
        var leadId = $("#lead").val();

        var data = {
            user_id: userId,
            lead_id: leadId,
            "_token": "{{ csrf_token() }}",
        };

        $.ajax({
            url: '/leads/register/'+leadId+'/'+userId,
            type: 'POST',
            data: data,
            success: function(data) {
                    $("#error").hide();
                if (data.success === 200) {

                    $("#alert").text("Lead assigned successfully!.");
                    window.location.reload();
                } else {
                    $("#alert").text("Error while assign the message!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                  $("#alert").hide();
                errorsHtml = "";
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);

                window.animateToTop("error");
                $("#error").show();
            }
        });
    });
});
</script>
@endsection