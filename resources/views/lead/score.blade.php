@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Lead Score</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Log</th>
                                <th>Score</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($scoreLogs as $score)
                            <tr>
                                <td>{{$score->log}}</td>
                                <td>{{$score->score}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection