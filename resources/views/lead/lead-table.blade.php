<div class="row justify-content-center">
    <div class="alert alert-success" style="display:none" id="alert" role="alert">
    </div>
    <div class="alert alert-danger" style="display:none" id="error" role="alert">
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Lead Table</div>
            <div class="card-body">
                <table class="table table-bordered" width="100%" id="lead-table">
                    <thead>
                        <tr>
                            <!-- <th>#</th> -->
                            <th>Created At</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Country</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Age</th>
                            <th>Feedback</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<div class="row" id="lead-export-table">
           <div class="col-md-12">
    <div class="card">
        <div class="card-header">Leads report</div>
        <div class="card-body">
     
                <form method="post" action='/leads/export'>
                    @csrf
                    <div class="form-group row">
                        <label for="export-lead-from" class="col-md-2 col-form-label text-md-right">{{ __('From') }}</label>
                        <div class="col-md-3">
                            <input id='export-lead-from' class='form-control' type="date" name="export-lead-from">
                            @if ($errors->has('export-lead-from'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('export-lead-from') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="export-lead-to" class="col-md-2 col-form-label text-md-right">{{ __('To') }}</label>
                        <div class="col-md-3">
                            <input id='export-lead-to' class='form-control' type="date" name="export-lead-to">
                            @if ($errors->has('export-lead-to'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('export-lead-to') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <input type="submit" style="margin-left: 36%;" class='form-control' name="submit" value="Export">
                        </div>
                    </div>
                </form>
    
        </div>
    </div>
    
    
</div>
</div>
<br>
<div class="col-md-4">
    <button id="hide-lead-export" class="btn btn-success">Hide report information</button>
</div>
<script>

function feedback(el){    

        var data = {
            "feedback":el.target.value,
            "id":el.target.getAttribute('data-leadId'),
            "_token": "{{ csrf_token() }}",
        };

    $.ajax({
        url: "/leads/feedback",
        method:'POST',
        data:data,
        success: function(data) {
                if (data.success === 200) {
                    $("#alert").text("Feedback set.");
                } else {
                    $("#alert").text("Error while setting feedback!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                errorsHtml = "";
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);
                window.animateToTop("error");
                $("#error").show();
            }
});
    
}
$(document).ready(function(){

    var leadExportInfoHidden = 0;
    $("#hide-lead-export").click(function(){
        if(!leadExportInfoHidden){
            $("#lead-export-table").hide();
            $("#hide-lead-export").html("Show report information");
            leadExportInfoHidden = 1;
        }
        else{
            $("#lead-export-table").show();
            $("#hide-lead-export").html("Hide report information");
            leadExportInfoHidden = 0;
        }
    });

    $('#lead-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url:'{!! route('lead.get') !!}',
            type: "POST",
            data:  {"_token": "{{ csrf_token() }}",}
        },
         dom: 'Bfrtip',
 buttons: [
    'copyHtml5', 'excelHtml5', 'pdfHtml5', 'csvHtml5'
  ],

        columns: [
			// { data: 'id', name: 'id'},
            { data: 'created_at', name: 'created_at' },
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'country', name: 'country',render:function(data){return data.name;} },    
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'age', name: 'age' },

            { data: 'feedback', name: 'feedback',render:function(data, type, row){

                var feedbacks = [
                    "Wrong number",
                    "Not answered",
                    "Answer",
                    "Interested",
                    "Not interested",
                    "Underage",
                    "Waiting",
                    "Deposit",
                    "Wrong data",
                    "Call again",
                ];
                 var html="<select onchange='feedback(event)' data-leadId='"+row.id+"'>";
                    html+="<option>";
                    html+="select a feedback";
                    html+="</option>";
                feedbacks.forEach(function(feedback){
                    
                    if(feedback === data){
                        html+="<option selected value='"+feedback+"'>";
                        html+=feedback;
                        html+="</option>";
                    }else{
                        html+="<option value='"+feedback+"'>";
                        html+=feedback;
                        html+="</option>";
                    }
                });
               
          
                html+="</select>";

                return html;
            } },
        ]
    });
});

</script>