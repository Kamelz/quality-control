
<table class="table">
<thead>
<tr>
    <th>#</th>
    @if(auth()->user()->is_admin)
    <th>Partner</th>
    @endif
    <th>Created At</th>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Country</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Age</th>
    @if(auth()->user()->is_admin)
    <th>Quality Score</th>
    @endif
    <th>Feedback</th>
</tr>
</thead>
<tbody>
    @foreach($leads as $lead)   
    <tr>
    <td>{{$lead->id}}</td>
    @if(auth()->user()->is_admin)
    <td>{{$lead->user_id}}</td>
    @endif
    <td>{{$lead->created_at}}</td>
    <td>{{$lead->first_name}}</td>
    <td>{{$lead->last_name}}</td>
    <td>{{$lead->country->name}}</td>
    <td>{{$lead->email}}</td>
    <td>{{$lead->phone}}</td>
    <td>{{$lead->age}}</td>
    @if(auth()->user()->is_admin)
    <td>{{$lead->quality_score}}</td>
    @endif
    <td>{{$lead->feedback}}</td>
    </tr>
    @endforeach
</tbody>
</table>

<script type="text/javascript">
  (function(){
          window.print();
  })();
</script>