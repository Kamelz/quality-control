<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Partners Table</div>
            <div class="card-body">
                <table class="table table-bordered" width="100%" id="users-table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Targeted Countries</th>
                            <th>Funds</th>
                            <th>Daily leads</th>
                            <th>Total leads</th>
                            <th>Action Price (CPL-CPA)</th>
                            <th>Target Quality Score From</th>
                            <th>Target Quality Score To</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row" id="user-export-table">
    <div class="col-md-12">
    <div class="card">
                 <div class="card-header">Partners Report</div>
            <div class="card-body">

        <form method="post" action='/users/export'>
            @csrf
            <div class="form-group row">
                <label for="export-lead-from" class="col-md-2 col-form-label text-md-right">{{ __('From') }}</label>
                <div class="col-md-3">
                    <input id='export-lead-from' class='form-control' type="date" name="export-lead-from">
                    @if ($errors->has('export-lead-from'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('export-lead-from') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="export-lead-to" class="col-md-2 col-form-label text-md-right">{{ __('To') }}</label>
                <div class="col-md-3">
                    <input id='export-lead-to' class='form-control' type="date" name="export-lead-to">
                    @if ($errors->has('export-lead-to'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('export-lead-to') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <input type="submit" style="margin-left: 36%;" class='form-control' name="submit" value="Export">
                </div>
            </div>
        </form>
    </div>
</div>
    </div>
</div>
<br>
  <div class="col-md-4">
        <button id="hide-user-export" class="btn btn-success">Hide report information</button>
    </div>
<script>
$(document).ready(function(){
    var userExportInfoHidden = 0;
	$("#hide-user-export").click(function(){
        if(!userExportInfoHidden){
            $("#user-export-table").hide();
            $("#hide-user-export").html("Show report information");
            userExportInfoHidden = 1;
        }
        else{
            $("#user-export-table").show();
            $("#hide-user-export").html("Hide report information");
            userExportInfoHidden = 0;
        }
    });
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('users.get') !!}',
        columns: [
			{ data: 'id', name: 'id' ,render:function (data){

				return "<a href='/users/"+data+"'>"+data+"</a>";
			}
			},
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'countries', name: 'countries', render:function (data){
                var countries = "";

                for (var country in data) {
                    countries +="<p> "+data[country].name+"</p> ";
                }

                $.each(data,function(country){
                    console.log(country);
                    
                })
                return "<p>"+countries+"</p>";
            } },
            { data: 'funds', name: 'funds' },
            { data: 'daily_leads', name: 'daily_leads' },
            { data: 'total_leads', name: 'total_leads' },
            { data: 'action_price', name: 'action_price' },
            { data: 'quality_score_target_from', name: 'quality_score_target_from' },
            { data: 'quality_score_target_to', name: 'quality_score_target_to' },
            { data: 'created_at', name: 'created_at' }
        ]
    });
});

</script>