@extends('layouts.app')
@section('content')

<style type="text/css">
    .message{
        flex:3;
    }   
    .date{
    flex:1;
    font-weight: 700;
    }
</style>

<div class="container">
            <div class="alert alert-success" style="display:none" id="alert" role="alert">
        </div>
        <div class="alert alert-danger" style="display:none" id="error" role="alert">
        </div>
    <div class="row justify-content-center">
    
        <div class="col-md-2">
            <div class="card">
                <div class="card-header">Users Messages</div>
                <div class="card-body">
                    <ul class="list-group">
                        @foreach($usersMessages as $user)
                        <li class="list-group-item">
                            <a onclick="showMessage(this)" href="#open{{$user->user->id}}">{{$user->user->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Send a Message</div>
                <div class="card-body">
                    <form class="form">
                        <div class="form-group">
                            <label for="name">Select Partner:</label>
                            <select class="form-control" id="user">
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea type="message" class="form-control" placeholder="Write a message.." id="message"></textarea>
                        </div>
                        <button type="button" id="send-message" class="btn btn-default">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
  @foreach($usersMessages as $user)
    <div class="row justify-content-center messages-box hide message-for-user{{$user->user->id}}" id="open{{$user->user->id}}">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Messages</div>
                <div class="card-body">
               
                    @foreach($user->message as $key => $value)
                     <p style="display: flex;"> <span class="message">{{$value}}</span> <span class="date">{{$key}}</span></p>
                     <hr>
                    @endforeach

                </div>
            </div>
        </div>
        
    </div>
@endforeach
</div>
<script>
    function showMessage(elem){
        $('.messages-box').hide();
        var link = elem.getAttribute("href");
        var itemId = link.substring(1, link.length);
        var userId = link.substring(5, link.length);
        $("#"+itemId).show('hide');
        
        $.ajax({
            url: '/mark-user-message-as-read/'+userId,
            type: 'POST',
            data: {"_token": $('meta[name="csrf-token"]').attr('content')}
        });

    }    
$(document).ready(function() {
    $('.messages-box').hide();

    $('#user').select2();
    $("#alert").css('display', 'none');
    $("#send-message").click(function(e) {
        var userId = $("#user").val();
        var message = $("#message").val();

        var data = {
            to: userId,
            message: message,
            "_token": "{{ csrf_token() }}",
        };
        $.ajax({
            url: '/messages/'+userId,
            type: 'POST',
            data: data,
            success: function(data) {
                $("#error").hide();
                if (data.success === 200) {
                    $("#alert").text("Message sent successfully!.");
                } else {
                    $("#alert").text("Error while sending the message!.");
                }
                window.animateToTop("alert");
                $("#alert").show();
            },
            error: function(data) {
                $("#alert").hide();
                errorsHtml = "";
                $.each(data.responseJSON.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                });
                $("#error").html(errorsHtml);
                window.animateToTop("error");
                $("#error").show();
            }
        });
    });
});
</script>
@endsection